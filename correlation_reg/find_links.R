library(dplyr)
library(stringr)
library(BSgenome.Hsapiens.UCSC.hg19)
library(BSgenome.Mmusculus.UCSC.mm10)
library(MotifDb)
library(doParallel)
library(TFBSTools)
library(JASPAR2016)
library(motifmatchr)
library(nnls)
library(yaml)

source("../utils/load_data.R")

args <- commandArgs(trailingOnly = T)
config_path <- args[1]
out_dir <- args[2]
result_path <- file.path(out_dir, "reg_result.csv")

config <- yaml.load_file(config_path)
data_dir <- config$data_dir
corr_dir <- config$corr_dir
n_cores <- config$n_cores
genome_code <- config$genome_code

processed_data <- load_processed_data(data_dir)
rna_counts <- processed_data$rna_counts
atac_counts <- processed_data$atac_counts
cell_types <- processed_data$cell_types

locus_data <- str_split_fixed(rownames(atac_counts), "-", 3)
grange_counts <- GRanges(seqnames = Rle(locus_data[, 1]),
                         ranges = IRanges(start = as.numeric(locus_data[, 2]),
                                          end = as.numeric(locus_data[, 3])))
names(grange_counts) <- rownames(atac_counts)

# From ChromVar
get_jaspar_motifs <- function(species = "Homo sapiens", collection = "CORE", ...) {
  opts <- list()
  opts["species"] <- species
  opts["collection"] <- collection
  opts <- c(opts, list(...))
  out <- TFBSTools::getMatrixSet(JASPAR2016::JASPAR2016, opts)
  if (!isTRUE(all.equal(TFBSTools::name(out), names(out)))) 
    names(out) <- paste(names(out), TFBSTools::name(out), sep = "_")
  return(out)
}

parse_motif_data <- function(lines) {
  motif_name <- str_extract(lines[1], "(?<=>).*?(?=_)")
  motif_name <- tolower(motif_name)
  motif_name <- paste0(
    toupper(substr(motif_name, 1, 1)),
    substr(motif_name, 2, nchar(motif_name))
  )
  motif_frequency <- as.numeric(unlist(sapply(lines[2:5], function(x) str_split(x, "\t"))))
  motif_matrix <- matrix(motif_frequency, nrow = 4, byrow = T, dimnames=list(c("A", "C", "G", "T")))
  
  return(
    PFMatrix(
      name = motif_name,
      profileMatrix = motif_matrix
    )
  )
}

get_hocomoco_motifs <- function() {
  all_lines <- readLines(config$hocomoco_path, warn = F)
  motif_data_list <- split(all_lines, ceiling(seq_along(all_lines) / 5))
  result = do.call(PFMatrixList, lapply(motif_data_list, function(x) parse_motif_data(x)))
  names(result) <- sapply(result, function(x) x@name)
  
  return(result)
}

fit_nnls <- function(overlaps, rna_counts, tfs_in_peaks) {
  cl <- makeCluster(n_cores)
  registerDoParallel(cl)

  all_genes <- unique(overlaps$gene_id)
  print(paste("Unique genes", length(all_genes))) 
  res <- foreach(current_gene = iter(all_genes), .combine = 'bind_rows',
                 .packages = c("nnls", "dplyr")) %dopar% {
    current_gene_tfs <- c()
    for(current_locus in overlaps[overlaps$gene_id == current_gene, ]$peak_id) {
      current_gene_tfs <- c(current_gene_tfs, tfs_in_peaks[[current_locus]])
    }
    current_gene_tfs <- unique(current_gene_tfs)
	is_tf_in_counts <- current_gene_tfs %in% rownames(rna_counts)
	current_gene_tfs <- current_gene_tfs[is_tf_in_counts]
	fit <- nnls(t(rna_counts[current_gene_tfs, ]), rna_counts[current_gene, ])
	
	if (length(current_gene_tfs[fit$x > 0]) > 0) {
	  result_tfs <- current_gene_tfs[fit$x > 0]  		
	} else {
	  result_tfs <- NA
	}
    current_gene_result <- data.frame(source = result_tfs, target = current_gene) 
	return(
    	current_gene_result  
    )
  }
  stopCluster(cl)
  return(res)
}

get_locus_tfs <- function(motif_ids, organism_code) {
  if(organism_code == "hg19") {
    tf_names <- str_extract(motif_ids, pattern = "(?<=_).*") %>% strsplit("::")
    return(
      gsub(r"(\(.*\))", "", tf_names) %>% unique()
    )
  } else {
    return(motif_ids)
  }
}

get_tfs_in_peaks <- function(genome_code) {
  if (genome_code == "hg19") {
    species_genome = BSgenome.Hsapiens.UCSC.hg19
    motifs <- get_jaspar_motifs()
  } else if (genome_code == "mm10") {
    species_genome = BSgenome.Mmusculus.UCSC.mm10
    motifs <- get_hocomoco_motifs()
  } else {
    stop("Invalid genome code.")
  }

  motif_ix <- matchMotifs(motifs, grange_counts, genome = species_genome)
  motif_matches <- motifMatches(motif_ix)
  motif_matches_df <- motif_matches %>% as.matrix %>% as.data.frame()
  motif_matches_df['peakID'] <- str_replace_all(rownames(motif_matches_df), "\\.", "-")
  result <- apply(motif_matches_df, 1,
	  function(x) { get_locus_tfs(colnames(motif_matches_df), genome_code)[x == T] })

  return(result)
}

tfs_in_peaks <- get_tfs_in_peaks(genome_code)
all_corr <- read.csv(file.path(corr_dir, "all_corr.csv"))

all_reg_results <- data.frame()

for (current_type in unique(cell_types)) {
	rna_counts_for_type <- rna_counts[, which(cell_types == current_type)] %>% as.matrix()
	corr_for_type <- all_corr[all_corr$cell_type == current_type, ]
	strong_corr <- corr_for_type[corr_for_type$rho > 0.25, ] 
	print(paste("Strong corr:", current_type, dim(strong_corr)[1]))

	cell_type_result <- fit_nnls(strong_corr, rna_counts_for_type, tfs_in_peaks)	
	cell_type_result$cell_type <- current_type
	all_reg_results <- bind_rows(all_reg_results, cell_type_result)
}

all_reg_results %>%
  na.omit() %>%
  write.csv(result_path)
