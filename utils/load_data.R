library(Matrix)

save_rna <- function(rna_data, dest_dir) {
	save_counts(rna_data, dest_dir, "RNA")	
}

save_atac <- function(atac_data, dest_dir) {
	save_counts(atac_data, dest_dir, "ATAC")
}

save_tsv <- function(data, file_path) {
	write.table(data, file_path, col.names = F, row.names = F, quote = F) 
}

save_counts <- function(counts_data, dest_dir, data_type) {
	print(paste(data_type, "matrix:", dim(counts_data)[1], "by", dim(counts_data)[2]))

	matrix_path <- file.path(dest_dir, "matrix.mtx")
	writeMM(counts_data, matrix_path)
	print(paste("+++ Saved", data_type, "matrix:", matrix_path))
	
	barcodes_path <- file.path(dest_dir, "barcodes.tsv")
	save_tsv(colnames(counts_data), barcodes_path)
	print(paste("+++ Saved", data_type, "ATAC barcodes:", barcodes_path))

	features_path <- file.path(dest_dir, "features.tsv")
	save_tsv(rownames(counts_data), features_path)		 
	print(paste("+++ Saved", data_type, "features:", features_path))
}

load_counts <- function(data_path, data_type) {
	counts <- readMM(file.path(data_path, data_type, "matrix.mtx"))	
	counts <- as(counts, "dgCMatrix")
	colnames(counts) <- readLines(file.path(data_path, data_type, "barcodes.tsv"))
	rownames(counts) <- readLines(file.path(data_path, data_type, "features.tsv"))
	
	return(counts)	
}
load_processed_data <- function(data_path) {
	rna_counts <- load_counts(data_path, "RNA")
	atac_counts <- load_counts(data_path, "ATAC") 
	cell_types <- readLines(file.path(data_path, "cell_types.tsv"))

	return(
		list(rna_counts = rna_counts, atac_counts = atac_counts, cell_types = cell_types)
	)
}
