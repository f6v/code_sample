## Master in bioinformatics thesis

### Introduction

This repository contains an example source code written by Ihor Filippov in the course of the master thesis. The main goal is to assess the gene regulatory network (GRN) inference algorithms on data [from parallel scRNA and scATAC measurements](https://doi.org/10.1038/s41592-019-0703-5). Parallel multimodal sequencing captures multiple molecular signal types from the same cell, which allows for establishing unambiguous links between, e.g. transcriptomic and epigenomic features (see picture below).

<img src="img/parallel_vs_regular.png"  width="600">

[SNARE-seq](https://doi.org/10.1038/s41587-019-0290-0) is a recent example of such a protocol based on UMIs. In this thesis, [~10k cells from adult mouse cortext](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE126074) are used to infer GRNs by correlating chromatin accessibility and gene expression, as well as mining for known motifs. This approach is based on [Liu et al. 2018](https://doi.org/10.1038/s41467-018-08205-7) and [Jin et al. 2020](https://doi.org/10.1186/s13059-020-1932-8). The workflow can be visualised as:

<img src="img/workflow.png"  width="600">

### Requirements

The code in this repository is meant to be run within [UGent HPC infrastructure](https://www.ugent.be/hpc/en). The dependencies are satisfied by loading pre-installed modules available at UGent HPC. Dependency management is, therefore, beyond the scope of this document. Nevertheless, here is an incomplete list of major packages used in the scripts:
- [Seurat 4](https://satijalab.org/seurat/) scRNA QC and normalisation
- [Signac](https://satijalab.org/signac/) scATAC QC and normalisation
- [doParallel](https://cran.r-project.org/web/packages/doParallel/index.html) multi-core parallelisation
- [motifmatchr](http://bioconductor.org/packages/release/bioc/html/motifmatchr.html) motif scanning
- [tidyverse](https://www.tidyverse.org) data wrangling, plotting, string processing

### Directory structure
```
.
├── preprocessing
│   ├── config.yaml
│   ├── prepare_SNARE.R
│   └── submit_job.pbs
├── correlation
│   ├── config.yaml
│   ├── get_correlations.R
│   └── submit_job.pbs
├── correlation_reg
│   ├── config.yaml
│   ├── find_links.R
│   └── submit_job.pbs
└── utils
    └── load_data.R
```
`preprocessing`,  `correlation`, and `correlation_reg` represent distinct analysis steps and are meant to run in this order. Each folder has a `submit_job.pbs` file, which specifies how to run a job on the HPC cluster. 
To submit a job, you need to do the following:
1. Navigate to the job directory, e.g. `preprocessing`:
```
cd preprocessing
```
2. Submit a job to the scheduler. On UGent HPC:
```
qsub submit_job.pbs
```
All folders contain `config.yaml`. This file is used to point a job to the required data. This is useful when you want to run a job with different sets of params.

### Analysis steps

##### Preprocessing

This step is based on [Signac tutorial](https://satijalab.org/signac/articles/snareseq.html).

`preprocessing` step involves the following:
1. Reading in both scRNA and scATAC data
2. Filtering based on number of reads per cell, the proportion of mitochondrial reads, number of fragments in TSS.
3. Normalisation. `Seurat`(negative binomial model) and `Signac`(TFIDF) are used to normalise scRNA and scATAC data, respectively.
4. Clustering and visualisation with `UMAP`.
5. Annotation using `Seurat` and reference dataset.
6. Exporting normalised and annotated data for further processing.

##### Correlation between scRNA and scATAC features

`correlation` computes Pearson correlation coefficient(PCC) between counts at accessible peaks and gene expression based on `preprocessing` results:
1. Peaks within 200kb around transcription start sites of genes are selected.
2. For each cell type, the peaks are retained if there are counts in at least 10% of that cell type's cells.
3. PCC is computed between gene expression and overlapping peak.
4. Results are saved for the next step.

##### Regulatory links based on correlations and motifs

`correlation_reg` uses `correlation` results:
1. Select correlations > 0.25, termed *strong correlation*.
2. Search motifs on known TFs using `motifmatchr` and [HOCOMOCO](https://hocomoco11.autosome.ru). Unlike Jaspar, HOCOMOCO has more motifs available for mice.
3. If there is a strong correlation between a gene and a peak, and a peak matches motif of a known TF, that TF is added as a *candidate regulator* of a gene.
4. For each gene, its expression level is regressed against those of candidate regulators.
5. Regulatory link is established if a candidate regulator's regression coefficient is > 0.

##### Validation of the results

Validation code is not included in this repository. However, the following steps can be taken:
- Compare inferred links to databases of known TF-target interactions, such as [Omnipath](https://omnipathdb.org). However, it is important to note that both Omnipath and HOCOMOCO/Jaspar are based on ChIP-seq experiments. Therefore, TF-target databases and motif databases probably do not provide independent evidence.
- Comparing inferred GRNs to protein-protein interaction databases, such as [STRING](https://string-db.org).
- Functional enrichment analysis of inferred regulons using [GO](http://geneontology.org/docs/go-enrichment-analysis/).











